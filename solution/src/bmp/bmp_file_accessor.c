#include "bmp/bmp_file_accessor.h"

#include <assert.h>

#include "bmp/bmp_header.h"
#include "bmp/bmp_utils.h"

// On error closes file automatically.
enum bmp_file_read_status bmp_file_open(struct bmp_file_accessor *bmp_file,
                                        const char *filename) {
    FILE *file = fopen(filename, "rb");
    if (file == NULL) {
        return BMP_FILE_READ_FILE_NOT_FOUND;
    }

    bmp_file->_file = file;

    struct bmp_header header;
    enum bmp_file_read_status status = bmp_file_read_header(bmp_file, &header);

    if (status != BMP_FILE_READ_OK) {
        fclose(file);
        return status;
    }

    status =
        bmp_file_read_status_from_header_status(bmp_header_validate(&header));

    if (status != BMP_FILE_READ_OK) {
        fclose(file);
        return status;
    }

    {
        uint32_t expected_file_size = header.data_offset + header.image_size;
        if (header.file_size < expected_file_size) {
            fclose(file);
            return BMP_FILE_READ_INVALID_FILE_SIZE;
        }

        fseek(file, 0, SEEK_END);
        uint32_t actual_file_size = ftell(file);
        if (actual_file_size < expected_file_size) {
            fclose(file);
            return BMP_FILE_READ_INVALID_FILE_SIZE;
        }
    }

    bmp_file->_width = header.width;
    bmp_file->_height = header.height;
    bmp_file->_data_offset = header.data_offset;

    return BMP_FILE_READ_OK;
}

enum bmp_file_read_status
bmp_file_read_header(const struct bmp_file_accessor *bmp_file,
                     struct bmp_header *header) {
    if (fseek(bmp_file->_file, 0, SEEK_SET) != 0) {
        return BMP_FILE_READ_INVALID_FILE;
    }

    if (fread(header, sizeof(*header), 1, bmp_file->_file) != 1) {
        return BMP_FILE_READ_INVALID_FILE;
    }

    return BMP_FILE_READ_OK;
}

void bmp_file_destroy(struct bmp_file_accessor *bmp_file) {
    fclose(bmp_file->_file);
}

enum bmp_file_read_status
bmp_file_get_pixel(const struct bmp_file_accessor *bmp_file, uint32_t x,
                   uint32_t y, struct pixel *pixel) {
    if (x >= bmp_file->_width || y >= bmp_file->_height) {
        return BMP_FILE_READ_INVALID_INPUT;
    }

    y = bmp_file->_height - y - 1;

    uint32_t stride =
        bmp_calculate_stride_in_bytes(bmp_file->_width, BMP_HEADER_BPP);
    uint32_t offset = bmp_file->_data_offset + y * stride + x * sizeof(*pixel);

    if (fseek(bmp_file->_file, offset, SEEK_SET) != 0) {
        return BMP_FILE_READ_INVALID_FILE;
    }

    if (fread(pixel, sizeof(*pixel), 1, bmp_file->_file) != 1) {
        return BMP_FILE_READ_INVALID_FILE;
    }

    return BMP_FILE_READ_OK;
}

enum bmp_file_read_status
bmp_file_get_block(const struct bmp_file_accessor *bmp_file, uint32_t x,
                   uint32_t y, uint32_t width, uint32_t height,
                   struct pixel *block) {
    if (x + width > bmp_file->_width || y + height > bmp_file->_height) {
        return BMP_FILE_READ_INVALID_INPUT;
    }

    y = bmp_file->_height - y - 1;

    uint32_t stride =
        bmp_calculate_stride_in_bytes(bmp_file->_width, BMP_HEADER_BPP);
    uint32_t offset = bmp_file->_data_offset + y * stride + x * sizeof(*block);

    for (uint32_t i = 0; i < height; ++i) {
        if (fseek(bmp_file->_file, offset - i * stride, SEEK_SET) != 0) {
            return BMP_FILE_READ_INVALID_FILE;
        }

        if (fread(block, sizeof(*block), width, bmp_file->_file) != width) {
            return BMP_FILE_READ_INVALID_FILE;
        }

        block += width;
    }

    return BMP_FILE_READ_OK;
}

uint32_t bmp_file_get_width(const struct bmp_file_accessor *bmp_file) {
    return bmp_file->_width;
}

uint32_t bmp_file_get_height(const struct bmp_file_accessor *bmp_file) {
    return bmp_file->_height;
}
