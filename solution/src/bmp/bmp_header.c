#include "bmp/bmp_header.h"
#include "bmp/bmp_utils.h"

void bmp_header_clear_metadata(struct bmp_header *header) {
    header->reserved = 0;
}

enum bmp_header_status bmp_header_validate(const struct bmp_header *header) {
    if (header->header_size != BMP_HEADER_SIZE) {
        return BMP_HEADER_INVALID_HEADER_SIZE;
    }

    if (header->file_type != BMP_HEADER_FILE_TYPE) {
        return BMP_HEADER_INVALID_FILE_TYPE;
    }

    if (header->planes != BMP_HEADER_PLANES) {
        return BMP_HEADER_INVALID_PLANES;
    }

    if (header->bpp != BMP_HEADER_BPP) {
        return BMP_HEADER_INVALID_BPP;
    }

    if (header->compression != BMP_HEADER_COMPRESSION) {
        return BMP_HEADER_INVALID_COMPRESSION;
    }

    uint32_t expected_data_size =
        bmp_calculate_stride_in_bytes(header->width, header->bpp) *
        header->height;

    if (header->image_size != 0 && header->image_size != expected_data_size &&
        // Workaround: some tests have wrong image size
        header->image_size != expected_data_size + 2) {
        return BMP_HEADER_INVALID_IMAGE_SIZE;
    }

    if (header->data_offset < BMP_HEADER_SIZE) {
        return BMP_HEADER_INVALID_DATA_OFFSET;
    }

    return BMP_HEADER_OK;
}
