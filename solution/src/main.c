#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp/bmp_file_accessor.h"
#include "bmp/bmp_file_write.h"
#include "error_messages.h"
#include "image_views/bmp_file_view.h"
#include "image_views/rotated90r_view.h"

DEF_ROTATED90R_VIEW(bmp_file)
DEF_ROTATED90R_VIEW(rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(rotated90r_rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)

IMPL_ROTATED90R_VIEW(bmp_file)
IMPL_ROTATED90R_VIEW(rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_ROTATED90R_VIEW(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)

DEF_BMP_FILE_WRITE(bmp_file)
DEF_BMP_FILE_WRITE(rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
DEF_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)

IMPL_BMP_FILE_WRITE(bmp_file)
IMPL_BMP_FILE_WRITE(rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)
IMPL_BMP_FILE_WRITE(
    rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file)

int main(int argc, char **argv) {

    //// User input ////

    if (argc != 4) {
        fprintf(stderr, "Usage: %s <input.bmp> <output.bmp> <angle>\n", argv[0]);
        return 1;
    }

    char *input_file_name = argv[1];
    char *output_file_name = argv[2];
    char *angle_str = argv[3];

    char *endptr;
    long angle = strtol(angle_str, &endptr, 10);
    if (*endptr != '\0') {
        fprintf(stderr, "Invalid angle: %s\n", angle_str);
        return 1;
    }

    if (angle < -270 || angle > 270) {
        fprintf(stderr, "Maybe it's prohibited to allow those angles \U0001F914\n");
    }

    angle %= 360;
    if (angle < 0) {
        angle += 360;
    }

    //// Reading input image ////

    enum bmp_file_read_status read_status;

    struct bmp_file_accessor bmp_file;
    read_status = bmp_file_open(&bmp_file, input_file_name);

    if (read_status != BMP_FILE_READ_OK) {
        fprintf(stderr, "Error reading bmp: %s\n", bmp_read_error_message(read_status));
        return 1;
    }

    struct bmp_header header;
    read_status = bmp_file_read_header(&bmp_file, &header);

    if (read_status != BMP_FILE_READ_OK) {
        bmp_file_destroy(&bmp_file);
        fprintf(stderr, "Error reading bmp: %s\n", bmp_read_error_message(read_status));
        return 1;
    }

    //// Rotating image ////

    struct bmp_file_view bmp_file_view = make_bmp_file_view(bmp_file);

    struct rotated90r_bmp_file_view rotated90r_bmp_file_view =
        make_rotated90r_bmp_file_view(bmp_file_view);
    struct rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_bmp_file_view(rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view);
    struct rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view
        rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view =
            make_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view);

    if (angle == 90 || angle == 270) {
        uint32_t tmp = header.x_ppm;
        header.x_ppm = header.y_ppm;
        header.y_ppm = tmp;
    }

    //// Writing rotated image ////

    enum bmp_file_write_status write_status;

    switch (angle) {
    case 0:
        write_status = bmp_file_block_write_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(output_file_name,
                                                    rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view, header);
        break;
    case 90:
        write_status = bmp_file_block_write_rotated90r_bmp_file_view(
            output_file_name, rotated90r_bmp_file_view, header);
        break;
    case 180:
        write_status = bmp_file_block_write_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
            output_file_name, rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view, header);
        break;
    case 270:
        write_status =
            bmp_file_block_write_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view(
                output_file_name,
                rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_rotated90r_bmp_file_view, header);
        break;
    default:
        fprintf(stderr, "Invalid angle: %ld\n", angle);
        fprintf(stderr, "Angle must be multiple of 90\n");
        return 1;
    }

    bmp_file_destroy(&bmp_file);

    if (write_status != BMP_FILE_WRITE_OK) {
        fprintf(stderr, "Error writing bmp: %s\n",
               bmp_write_error_message(write_status));
        return 1;
    }

    return 0;
}
