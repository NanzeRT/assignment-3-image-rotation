#include "image_views/bmp_file_view.h"

#include <assert.h>

struct bmp_file_view make_bmp_file_view(struct bmp_file_accessor bmp_file) {
    return (struct bmp_file_view){bmp_file};
}

struct pixel bmp_file_view_get_pixel(const struct bmp_file_view *view,
                                     uint64_t x, uint64_t y) {
    struct pixel pixel;
    assert(bmp_file_get_pixel(&view->_bmp_file, x, y, &pixel) ==
           BMP_FILE_READ_OK);
    return pixel;
}

void bmp_file_view_get_block(const struct bmp_file_view *view, uint64_t x,
                             uint64_t y, uint64_t width, uint64_t height,
                             struct pixel *restrict *restrict block,
                             struct pixel *restrict *restrict buf) {
    (void)buf; // supress 'unused parameter' warning
    assert(bmp_file_get_block(&view->_bmp_file, x, y, width, height, *block) ==
           BMP_FILE_READ_OK);
}

void bmp_file_view_set_pixel(struct bmp_file_view *view, uint64_t x, uint64_t y,
                             struct pixel pixel) {
    (void)view;
    (void)x;
    (void)y;
    (void)pixel; // supress 'unused parameters' warning
    assert(!"Not implemented");
}

uint64_t bmp_file_view_get_width(const struct bmp_file_view *view) {
    return bmp_file_get_width(&view->_bmp_file);
}

uint64_t bmp_file_view_get_height(const struct bmp_file_view *view) {
    return bmp_file_get_height(&view->_bmp_file);
}
