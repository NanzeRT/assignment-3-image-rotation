#include "image_views/image_view.h"

struct image_view make_image_view(struct image *image) {
    return (struct image_view){image};
}

struct pixel image_view_get_pixel(const struct image_view *view, uint64_t x,
                                  uint64_t y) {
    return view->_image->data[y * view->_image->width + x];
}

void image_view_get_block(const struct image_view *view, uint64_t x, uint64_t y,
                          uint64_t width, uint64_t height,
                          struct pixel *restrict *restrict block,
                          struct pixel *restrict *restrict buf) {
    (void)buf; // supress 'unused parameter' warning
    for (uint64_t i = 0; i < height; ++i) {
        for (uint64_t j = 0; j < width; ++j) {
            (*block)[i * width + j] = image_view_get_pixel(view, x + j, y + i);
        }
    }
}

void image_view_set_pixel(struct image_view *view, uint64_t x, uint64_t y,
                          struct pixel pixel) {
    view->_image->data[y * view->_image->width + x] = pixel;
}

uint64_t image_view_get_width(const struct image_view *view) {
    return view->_image->width;
}

uint64_t image_view_get_height(const struct image_view *view) {
    return view->_image->height;
}
