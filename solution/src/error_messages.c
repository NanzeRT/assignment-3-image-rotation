#include "error_messages.h"
#include "bmp/bmp_file_accessor.h"
#include "bmp/bmp_file_write.h"

char *bmp_write_error_message(enum bmp_file_write_status status) {
    switch (status) {
    case BMP_FILE_WRITE_OK:
        return "success";
    case BMP_FILE_WRITE_ERROR:
        return "error";
    case BMP_FILE_WRITE_INVALID_FILE:
        return "invalid file";
    case BMP_FILE_WRITE_INVALID_HEADER:
        return "invalid header";
    }

    assert(!"Invalid status");
}

char *bmp_read_error_message(enum bmp_file_read_status status) {
    switch (status) {
    case BMP_FILE_READ_OK:
        return "success";
    case BMP_FILE_READ_FILE_NOT_FOUND:
        return "file not found";
    case BMP_FILE_READ_INVALID_FILE:
        return "invalid file";
    case BMP_FILE_READ_INVALID_FILE_SIZE:
        return "invalid file size";
    case BMP_FILE_READ_INVALID_SIGNATURE:
        return "invalid signature";
    case BMP_FILE_READ_INVALID_BITS_PER_PIXEL:
        return "invalid bits per pixel";
    case BMP_FILE_READ_INVALID_COMPRESSION:
        return "invalid compression";
    case BMP_FILE_READ_INVALID_IMAGE_SIZE:
        return "invalid image size";
    case BMP_FILE_READ_INVALID_DATA_OFFSET:
        return "invalid data offset";
    case BMP_FILE_READ_INVALID_PLANES:
        return "invalid planes";
    case BMP_FILE_READ_INVALID_INPUT:
        return "invalid input";
    case BMP_FILE_READ_INVALID_HEADER_SIZE:
        return "invalid header size";
    }

    assert(!"Invalid status");
}
