#pragma once

#include "bmp/bmp_file_accessor.h"
#include "pixel.h"

struct bmp_file_view {
    struct bmp_file_accessor _bmp_file;
};

struct bmp_file_view make_bmp_file_view(struct bmp_file_accessor bmp_file);
struct pixel bmp_file_view_get_pixel(const struct bmp_file_view *view,
                                     uint64_t x, uint64_t y);
void bmp_file_view_get_block(const struct bmp_file_view *view,
                                      uint64_t x, uint64_t y, uint64_t width,
                                      uint64_t height,
                                      struct pixel *restrict *restrict block,
                                      struct pixel *restrict *restrict buf);
void bmp_file_view_set_pixel(struct bmp_file_view *view, uint64_t x, uint64_t y,
                             struct pixel pixel);
uint64_t bmp_file_view_get_width(const struct bmp_file_view *view);
uint64_t bmp_file_view_get_height(const struct bmp_file_view *view);
