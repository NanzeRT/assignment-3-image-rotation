#pragma once

#include "image_utils.h"
#include "pixel.h"

#define DEF_ROTATED90R_VIEW(base)                                              \
    struct rotated90r_##base##_view {                                          \
        struct base##_view _view;                                              \
    };                                                                         \
                                                                               \
    struct rotated90r_##base##_view make_rotated90r_##base##_view(             \
        struct base##_view view);                                              \
    struct pixel rotated90r_##base##_view_get_pixel(                           \
        const struct rotated90r_##base##_view *view, uint64_t x, uint64_t y);  \
    void rotated90r_##base##_view_set_pixel(                                   \
        struct rotated90r_##base##_view *view, uint64_t x, uint64_t y,         \
        struct pixel pixel);                                                   \
    uint64_t rotated90r_##base##_view_get_width(                               \
        const struct rotated90r_##base##_view *view);                          \
    uint64_t rotated90r_##base##_view_get_height(                              \
        const struct rotated90r_##base##_view *view);

#define IMPL_ROTATED90R_VIEW(base)                                             \
    struct rotated90r_##base##_view make_rotated90r_##base##_view(             \
        struct base##_view view) {                                             \
        return (struct rotated90r_##base##_view){                              \
            ._view = view,                                                     \
        };                                                                     \
    }                                                                          \
                                                                               \
    struct pixel rotated90r_##base##_view_get_pixel(                           \
        const struct rotated90r_##base##_view *view, uint64_t x, uint64_t y) { \
        return base##_view_get_pixel(                                          \
            &view->_view, y, base##_view_get_height(&view->_view) - x - 1);    \
    }                                                                          \
                                                                               \
    void rotated90r_##base##_view_get_block(                                   \
        const struct rotated90r_##base##_view *view, uint64_t x, uint64_t y,   \
        uint64_t width, uint64_t height,                                       \
        struct pixel *restrict *restrict block,                                \
        struct pixel *restrict *restrict buf) {                                \
        base##_view_get_block(                                                 \
            &view->_view, y, base##_view_get_height(&view->_view) - x - width, \
            height, width, block, buf);                                        \
        rotate90r(*block, *buf, width, height);                                \
        struct pixel *tmp = *block;                                            \
        *block = *buf;                                                         \
        *buf = tmp;                                                            \
    }                                                                          \
                                                                               \
    void rotated90r_##base##_view_set_pixel(                                   \
        struct rotated90r_##base##_view *view, uint64_t x, uint64_t y,         \
        struct pixel pixel) {                                                  \
        base##_view_set_pixel(&view->_view, y,                                 \
                              base##_view_get_height(&view->_view) - x - 1,    \
                              pixel);                                          \
    }                                                                          \
                                                                               \
    uint64_t rotated90r_##base##_view_get_width(                               \
        const struct rotated90r_##base##_view *view) {                         \
        return base##_view_get_height(&view->_view);                           \
    }                                                                          \
                                                                               \
    uint64_t rotated90r_##base##_view_get_height(                              \
        const struct rotated90r_##base##_view *view) {                         \
        return base##_view_get_width(&view->_view);                            \
    }
