#pragma once

#include "image.h"
#include "pixel.h"

// serves as api reference
struct image_view {
    struct image *_image;
};

struct image_view make_image_view(struct image *image);
struct pixel image_view_get_pixel(const struct image_view *view, uint64_t x,
                                  uint64_t y);
void image_view_get_block(const struct image_view *view, uint64_t x, uint64_t y,
                          uint64_t width, uint64_t height,
                          struct pixel *restrict *restrict block,
                          struct pixel *restrict *restrict buf);
void image_view_set_pixel(struct image_view *view, uint64_t x, uint64_t y,
                          struct pixel pixel);
uint64_t image_view_get_width(const struct image_view *view);
uint64_t image_view_get_height(const struct image_view *view);
