#pragma once

#include "bmp/bmp_file_accessor.h"
#include "bmp/bmp_file_write.h"

char *bmp_write_error_message(enum bmp_file_write_status status);
char *bmp_read_error_message(enum bmp_file_read_status status);
