#pragma once

#include <assert.h>
#include <stdint.h>

static inline uint32_t bmp_calculate_stride_in_bytes(uint32_t width,
                                                     uint32_t bits_per_pixel) {
    assert(bits_per_pixel % 8 == 0);
    uint32_t bytes_per_pixel = bits_per_pixel / 8;
    uint32_t stride = width * bytes_per_pixel;
    stride = (stride + 3) & ~3;
    return stride;
}
