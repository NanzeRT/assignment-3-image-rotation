#pragma once

#include <stdbool.h>
#include <stdio.h>
#include <sys/cdefs.h>

#include "bmp/bmp_header.h"
#include "bmp/bmp_utils.h"
#include "image_views/image_view.h"

static const uint32_t BLOCK_WIDTH = 128;
static const uint32_t BLOCK_HEIGHT = 128;
static const uint32_t BLOCK_SIZE = BLOCK_WIDTH * BLOCK_HEIGHT;

enum bmp_file_write_status {
    BMP_FILE_WRITE_OK,
    BMP_FILE_WRITE_INVALID_FILE,
    BMP_FILE_WRITE_INVALID_HEADER,
    BMP_FILE_WRITE_ERROR,
};

#define DEF_BMP_FILE_WRITE(base)                                               \
    enum bmp_file_write_status bmp_file_write_##base##_view(                   \
        const char *filename, const struct base##_view view,                   \
        struct bmp_header header);

#define IMPL_BMP_FILE_WRITE(base)                                              \
    enum bmp_file_write_status bmp_file_write_##base##_view(                   \
        const char *filename, const struct base##_view view,                   \
        struct bmp_header header) {                                            \
        uint32_t width = base##_view_get_width(&view);                         \
        uint32_t height = base##_view_get_height(&view);                       \
        uint32_t data_offset = BMP_HEADER_DATA_OFFSET;                         \
        uint32_t stride = bmp_calculate_stride_in_bytes(width, header.bpp);    \
        uint32_t image_size = stride * height;                                 \
        uint32_t padding = stride - width * header.bpp / 8;                    \
                                                                               \
        header.width = width;                                                  \
        header.height = height;                                                \
        header.data_offset = data_offset;                                      \
        header.image_size = image_size;                                        \
        header.file_size = data_offset + image_size;                           \
                                                                               \
        enum bmp_header_status status = bmp_header_validate(&header);          \
                                                                               \
        if (status != BMP_HEADER_OK) {                                         \
            return BMP_FILE_WRITE_INVALID_HEADER;                              \
        }                                                                      \
                                                                               \
        FILE *file = fopen(filename, "wb");                                    \
                                                                               \
        if (file == NULL) {                                                    \
            return BMP_FILE_WRITE_INVALID_FILE;                                \
        }                                                                      \
                                                                               \
        if (fwrite(&header, sizeof(header), 1, file) != 1) {                   \
            fclose(file);                                                      \
            return BMP_FILE_WRITE_ERROR;                                       \
        }                                                                      \
                                                                               \
        /* NOTICE: `y < height` is valid because of overflow */                \
        for (uint32_t y = height - 1; y < height; --y) {                       \
            for (uint32_t x = 0; x < width; ++x) {                             \
                struct pixel pixel = base##_view_get_pixel(&view, x, y);       \
                if (fwrite(&pixel, sizeof(pixel), 1, file) != 1) {             \
                    fclose(file);                                              \
                    return BMP_FILE_WRITE_ERROR;                               \
                }                                                              \
            }                                                                  \
            if (padding != 0) {                                                \
                const uint8_t padding_bytes[4] = {0};                          \
                if (fwrite(&padding_bytes, 1, padding, file) != padding) {     \
                    fclose(file);                                              \
                    return BMP_FILE_WRITE_ERROR;                               \
                }                                                              \
            }                                                                  \
        }                                                                      \
                                                                               \
        return BMP_FILE_WRITE_OK;                                              \
    }                                                                          \
                                                                               \
    enum bmp_file_write_status bmp_file_block_write_##base##_view(             \
        const char *filename, const struct base##_view view,                   \
        struct bmp_header header) {                                            \
        uint32_t width = base##_view_get_width(&view);                         \
        uint32_t height = base##_view_get_height(&view);                       \
        uint32_t data_offset = BMP_HEADER_DATA_OFFSET;                         \
        uint32_t stride = bmp_calculate_stride_in_bytes(width, header.bpp);    \
        uint32_t image_size = stride * height;                                 \
        uint32_t padding = stride - width * header.bpp / 8;                    \
                                                                               \
        header.width = width;                                                  \
        header.height = height;                                                \
        header.data_offset = data_offset;                                      \
        header.image_size = image_size;                                        \
        header.file_size = data_offset + image_size;                           \
                                                                               \
        enum bmp_header_status status = bmp_header_validate(&header);          \
                                                                               \
        if (status != BMP_HEADER_OK) {                                         \
            return BMP_FILE_WRITE_INVALID_HEADER;                              \
        }                                                                      \
                                                                               \
        FILE *file = fopen(filename, "wb");                                    \
                                                                               \
        if (file == NULL) {                                                    \
            return BMP_FILE_WRITE_INVALID_FILE;                                \
        }                                                                      \
                                                                               \
        if (fwrite(&header, sizeof(header), 1, file) != 1) {                   \
            fclose(file);                                                      \
            return BMP_FILE_WRITE_ERROR;                                       \
        }                                                                      \
                                                                               \
        struct pixel _block[BLOCK_SIZE];                                       \
        struct pixel _buf[BLOCK_SIZE];                                         \
        struct pixel *block = _block;                                          \
        struct pixel *buf = _buf;                                              \
                                                                               \
        uint32_t y = 0;                                                        \
        while (y + BLOCK_HEIGHT < height) {                                    \
            uint32_t x = 0;                                                    \
            while (x + BLOCK_WIDTH < width) {                                  \
                base##_view_get_block(&view, x, y, BLOCK_WIDTH, BLOCK_HEIGHT,  \
                                      &block, &buf);                           \
                if (!_bmp_file_write_block(file, x, y, width, height, stride,  \
                                           data_offset, BLOCK_WIDTH,           \
                                           BLOCK_HEIGHT, block)) {             \
                    fclose(file);                                              \
                    return BMP_FILE_WRITE_ERROR;                               \
                }                                                              \
                x += BLOCK_WIDTH;                                              \
            }                                                                  \
            base##_view_get_block(&view, x, y, width - x, BLOCK_HEIGHT,        \
                                  &block, &buf);                               \
            if (!_bmp_file_write_block_with_padding(                           \
                    file, x, y, width, height, stride, data_offset, width - x, \
                    BLOCK_HEIGHT, padding, block)) {                           \
                fclose(file);                                                  \
                return BMP_FILE_WRITE_ERROR;                                   \
            }                                                                  \
            y += BLOCK_HEIGHT;                                                 \
        }                                                                      \
        uint32_t x = 0;                                                        \
        while (x + BLOCK_WIDTH < width) {                                      \
            base##_view_get_block(&view, x, y, BLOCK_WIDTH, height - y,        \
                                  &block, &buf);                               \
            if (!_bmp_file_write_block(file, x, y, width, height, stride,      \
                                       data_offset, BLOCK_WIDTH, height - y,   \
                                       block)) {                               \
                fclose(file);                                                  \
                return BMP_FILE_WRITE_ERROR;                                   \
            }                                                                  \
            x += BLOCK_WIDTH;                                                  \
        }                                                                      \
        base##_view_get_block(&view, x, y, width - x, height - y, &block,      \
                              &buf);                                           \
        if (!_bmp_file_write_block_with_padding(                               \
                file, x, y, width, height, stride, data_offset, width - x,     \
                height - y, padding, block)) {                                 \
            fclose(file);                                                      \
            return BMP_FILE_WRITE_ERROR;                                       \
        }                                                                      \
                                                                               \
        return BMP_FILE_WRITE_OK;                                              \
    }

bool static inline _bmp_file_write_block(FILE *file, int32_t x, int32_t y,
                                         int32_t width, int32_t height,
                                         int32_t stride, int32_t data_offset,
                                         int32_t block_width,
                                         int32_t block_height,
                                         const struct pixel *block) {
    (void)width;
    for (int32_t i = 0; i < block_height; ++i) {
        fseek(file,
              data_offset + (height - y - i - 1) * stride +
                  x * (int32_t)sizeof(struct pixel),
              SEEK_SET);
        if (fwrite(&block[i * block_width], sizeof(struct pixel), block_width,
                   file) != block_width) {
            return false;
        }
    }
    return true;
}

bool static inline _bmp_file_write_block_with_padding(
    FILE *file, int32_t x, int32_t y, int32_t width, int32_t height,
    int32_t stride, int32_t data_offset, int32_t block_width,
    int32_t block_height, int32_t padding, const struct pixel *block) {
    (void)width;
    for (int32_t i = 0; i < block_height; ++i) {
        fseek(file,
              data_offset + (height - y - i - 1) * stride +
                  x * (int32_t)sizeof(struct pixel),
              SEEK_SET);
        if (fwrite(&block[i * block_width], sizeof(struct pixel), block_width,
                   file) != block_width) {
            return false;
        }
        const uint8_t padding_bytes[4] = {0};
        if (fwrite(&padding_bytes, 1, padding, file) != padding) {
            return false;
        }
    }
    return true;
}
