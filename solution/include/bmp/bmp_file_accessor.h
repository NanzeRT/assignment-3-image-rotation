#pragma once

#include <assert.h>
#include <stdint.h>
#include <stdio.h>

#include "bmp_header.h"
#include "pixel.h"

// SAFETY: bmp header is immutable here
struct bmp_file_accessor {
    FILE *_file;
    uint32_t _width;
    uint32_t _height;
    uint32_t _data_offset;
};

enum bmp_file_read_status {
    BMP_FILE_READ_OK,
    BMP_FILE_READ_INVALID_SIGNATURE,
    BMP_FILE_READ_INVALID_BITS_PER_PIXEL,
    BMP_FILE_READ_INVALID_COMPRESSION,
    BMP_FILE_READ_INVALID_IMAGE_SIZE,
    BMP_FILE_READ_INVALID_HEADER_SIZE,
    BMP_FILE_READ_INVALID_DATA_OFFSET,
    BMP_FILE_READ_INVALID_PLANES,
    BMP_FILE_READ_INVALID_FILE_SIZE,
    BMP_FILE_READ_INVALID_FILE,
    BMP_FILE_READ_FILE_NOT_FOUND,
    BMP_FILE_READ_INVALID_INPUT,
};

static inline enum bmp_file_read_status
bmp_file_read_status_from_header_status(enum bmp_header_status status) {
    switch (status) {
    case BMP_HEADER_OK:
        return BMP_FILE_READ_OK;
    case BMP_HEADER_INVALID_HEADER_SIZE:
        return BMP_FILE_READ_INVALID_HEADER_SIZE;
    case BMP_HEADER_INVALID_FILE_TYPE:
        return BMP_FILE_READ_INVALID_SIGNATURE;
    case BMP_HEADER_INVALID_PLANES:
        return BMP_FILE_READ_INVALID_PLANES;
    case BMP_HEADER_INVALID_BPP:
        return BMP_FILE_READ_INVALID_BITS_PER_PIXEL;
    case BMP_HEADER_INVALID_COMPRESSION:
        return BMP_FILE_READ_INVALID_COMPRESSION;
    case BMP_HEADER_INVALID_IMAGE_SIZE:
        return BMP_FILE_READ_INVALID_IMAGE_SIZE;
    case BMP_HEADER_INVALID_DATA_OFFSET:
        return BMP_FILE_READ_INVALID_DATA_OFFSET;
    }

    assert(0);
}

enum bmp_file_read_status bmp_file_open(struct bmp_file_accessor *bmp_file,
                                        const char *filename);

void bmp_file_destroy(struct bmp_file_accessor *bmp_file);

enum bmp_file_read_status
bmp_file_read_header(const struct bmp_file_accessor *bmp_file,
                     struct bmp_header *header);
enum bmp_file_read_status
bmp_file_get_pixel(const struct bmp_file_accessor *bmp_file, uint32_t x,
                   uint32_t y, struct pixel *pixel);
enum bmp_file_read_status
bmp_file_get_block(const struct bmp_file_accessor *bmp_file, uint32_t x,
                   uint32_t y, uint32_t width, uint32_t height,
                   struct pixel *block);
uint32_t bmp_file_get_width(const struct bmp_file_accessor *bmp_file);
uint32_t bmp_file_get_height(const struct bmp_file_accessor *bmp_file);
