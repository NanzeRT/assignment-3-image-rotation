#pragma once

#include "pixel.h"
#include <stdint.h>

// BITMAPINFOHEADER
// https://en.wikipedia.org/wiki/BMP_file_format
struct __attribute__((packed)) bmp_header {
    uint16_t file_type;
    uint32_t file_size;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t header_size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bpp;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_ppm;
    uint32_t y_ppm;
    uint32_t colors_used;
    uint32_t colors_important;
};

enum bmp_header_status {
    BMP_HEADER_OK,
    BMP_HEADER_INVALID_HEADER_SIZE,
    BMP_HEADER_INVALID_FILE_TYPE,
    BMP_HEADER_INVALID_PLANES,
    BMP_HEADER_INVALID_BPP,
    BMP_HEADER_INVALID_COMPRESSION,
    BMP_HEADER_INVALID_IMAGE_SIZE,
    BMP_HEADER_INVALID_DATA_OFFSET,
};

static const uint16_t BMP_HEADER_FILE_TYPE = 0x4D42; // "BM"
static const uint32_t BMP_HEADER_DATA_OFFSET = 54;
static const uint32_t BMP_HEADER_SIZE = 40;
static const uint16_t BMP_HEADER_PLANES = 1;
static const uint16_t BMP_HEADER_BPP = sizeof(struct pixel) * 8;
static const uint32_t BMP_HEADER_COMPRESSION = 0;

void bmp_header_clear_metadata(struct bmp_header *header);
enum bmp_header_status bmp_header_validate(const struct bmp_header *header);
