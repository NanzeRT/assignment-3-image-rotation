#pragma once

#include <stdint.h>

#include "pixel.h"

static void rotate90r(struct pixel *restrict in, struct pixel *restrict out,
                      uint64_t out_width, uint64_t out_height) {
    for (uint64_t i = 0; i < out_height; ++i) {
        for (uint64_t j = 0; j < out_width; ++j) {
            out[i * out_width + j] = in[(out_width - j - 1) * out_height + i];
        }
    }
}
